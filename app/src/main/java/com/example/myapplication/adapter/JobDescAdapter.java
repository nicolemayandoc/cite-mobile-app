package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.modals.JobDescriptionModal;

import org.w3c.dom.Text;

import java.security.PublicKey;
import java.util.ArrayList;

public class JobDescAdapter extends RecyclerView.Adapter<JobDescAdapter.JobDescHolder> {

    public ArrayList<JobDescriptionModal> jobModal;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public static class JobDescHolder extends RecyclerView.ViewHolder{

        public ImageView companyImageLogo;
        public TextView positionName;
        public TextView companyName;
        public TextView locationAddress;
        public TextView salaryOffer;
        public TextView employmentType;
        public TextView jobDesc;
        public TextView datePosted;
        public JobDescHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            companyImageLogo = itemView.findViewById(R.id.companyImageLogo);
            positionName = itemView.findViewById(R.id.positionTitle);
            companyName = itemView.findViewById(R.id.companyName);
            locationAddress = itemView.findViewById(R.id.locationAddress);
            salaryOffer = itemView.findViewById(R.id.salaryOffer);
            employmentType = itemView.findViewById(R.id.employmentType);
            jobDesc = itemView.findViewById(R.id.jobDescription);
            datePosted = itemView.findViewById(R.id.datePosted);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });


        }
    }

    public JobDescAdapter(ArrayList<JobDescriptionModal> modals){
        jobModal = modals;


    }

    @NonNull
    @Override
    public JobDescHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_list, parent, false);
        JobDescHolder holder = new JobDescHolder(view, mListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull JobDescHolder holder, int position) {
        JobDescriptionModal jobItems = jobModal.get(position);

        holder.companyImageLogo.setImageResource(jobItems.getLogoCompanyImage());
        holder.positionName.setText(jobItems.getPositionName());
        holder.companyName.setText(jobItems.getCompanyName());
        holder.locationAddress.setText(jobItems.getLocationName());
        holder.salaryOffer.setText(jobItems.getSalaryOffer());
        holder.employmentType.setText(jobItems.getEmploymentType());
        holder.jobDesc.setText(jobItems.getJobDescription());
        holder.datePosted.setText(jobItems.getDatePosted());

    }

    @Override
    public int getItemCount() {
        return jobModal.size();
    }
}

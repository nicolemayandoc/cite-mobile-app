package com.example.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.JobDescriptionView;
import com.example.myapplication.R;
import com.example.myapplication.adapter.JobDescAdapter;
import com.example.myapplication.modals.JobDescriptionModal;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private JobDescAdapter jobAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ArrayList<JobDescriptionModal> jobModal = new ArrayList<>();
        jobModal.add(new JobDescriptionModal(R.drawable.sample_logo,
                "Software Developer", "Apple Inc.",
                "Dagupan City, Pangasinan",
                "P 10, 0000 - 16, 000 / Month,", "Full Time",
                "We are looking for a Front End React developerto join our fast-growing team..."
                ,"Dec 01, 2022"));
        jobModal.add(new JobDescriptionModal(R.drawable.samplelogo2,
                "UI/UX Designer", "Volkswagen",
                "Urdaneta City, Pangasinan",
                "P 20, 0000 - 36, 000 / Month,", "Full Time",
                "We are looking for a Front End React developerto join our fast-growing team..."
                ,"Dec 01, 2022"));

        jobModal.add(new JobDescriptionModal(R.drawable.sample_logo,
                "Software Developer", "Apple Inc.",
                "Dagupan City, Pangasinan",
                "P 10, 0000 - 16, 000 / Month,", "Full Time",
                "We are looking for a Front End React developerto join our fast-growing team..."
                ,"Dec 01, 2022"));

        recyclerView = view.findViewById(R.id.recyclerView_Home);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        jobAdapter = new JobDescAdapter(jobModal);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(jobAdapter);


        jobAdapter.setOnItemClickListener(new JobDescAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getContext(), JobDescriptionView.class);
                getActivity().startActivity(intent);
            }
        });





        return view;

    }
}

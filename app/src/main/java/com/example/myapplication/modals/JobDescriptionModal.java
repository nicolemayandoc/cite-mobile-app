package com.example.myapplication.modals;

import android.media.Image;

public class JobDescriptionModal {
    private int logoCompanyImage;
    private String positionName;
    private String companyName;
    private String locationName;
    private String salaryOffer;
    private String employmentType;
    private String jobDescription;
    private String datePosted;

    public JobDescriptionModal(int logoCompanyImage, String positionName, String companyName, String locationName, String salaryOffer, String employmentType, String jobDescription, String datePosted) {
        this.logoCompanyImage = logoCompanyImage;
        this.positionName = positionName;
        this.companyName = companyName;
        this.locationName = locationName;
        this.salaryOffer = salaryOffer;
        this.employmentType = employmentType;
        this.jobDescription = jobDescription;
        this.datePosted = datePosted;
    }


    public int getLogoCompanyImage() {
        return logoCompanyImage;
    }

    public void setLogoCompanyImage(int logoCompanyImage) {
        this.logoCompanyImage = logoCompanyImage;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSalaryOffer() {
        return salaryOffer;
    }

    public void setSalaryOffer(String salaryOffer) {
        this.salaryOffer = salaryOffer;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }
}
